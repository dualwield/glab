package constructor

import (
	"net/http"
	"net/url"
)

type API struct {
	Client *http.Client
	Token  string
	URL    url.URL
}
