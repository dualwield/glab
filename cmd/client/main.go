package main

import (
	"fmt"
	"gitlab.com/dualwield/glab/gitlab"
	"gitlab.com/dualwield/glab/groups"
	z "gitlab.com/dualwield/golib/zodiac"
	"net/http"
	"net/url"
	"os"
)

func main() {
	api := gitlab.API{
		Client: &http.Client{},
		Token:  z.Lookup("GITLAB_TOKEN").Val(),
		URL: func() url.URL {
			u, err := url.Parse(z.Lookup("GITLAB_URL").Else("https://gitlab.com"))
			if err != nil {
				fmt.Println(err) //TODO propper logging
				os.Exit(1)
			}
			fmt.Println(u.String())
			return *u
		}(),
	}

	groupsCall := groups.SubList(api, 4306911).WithStatistics().SkipGroups([]int{14905506, 5506185})
	fmt.Println(groupsCall.Endpoint())
	gxs, err := groupsCall.Paginate()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, g := range gxs {
		fmt.Println(g.Name)
	}

}
