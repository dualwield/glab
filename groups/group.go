package group

import "time"

type Group struct {
	ID                             int         `json:"id,omitempty"`
	Name                           string      `json:"name,omitempty"`
	Path                           string      `json:"path,omitempty"`
	Description                    string      `json:"description,omitempty"`
	Visibility                     string      `json:"visibility,omitempty"`
	ShareWithGroupLock             bool        `json:"share_with_group_lock,omitempty"`
	RequireTwoFactorAuthentication bool        `json:"require_two_factor_authentication,omitempty"`
	TwoFactorGracePeriod           int         `json:"two_factor_grace_period,omitempty"`
	ProjectCreationLevel           string      `json:"project_creation_level,omitempty"`
	AutoDevopsEnabled              interface{} `json:"auto_devops_enabled,omitempty"`
	SubgroupCreationLevel          string      `json:"subgroup_creation_level,omitempty"`
	EmailsDisabled                 interface{} `json:"emails_disabled,omitempty"`
	MentionsDisabled               interface{} `json:"mentions_disabled,omitempty"`
	LfsEnabled                     bool        `json:"lfs_enabled,omitempty"`
	DefaultBranchProtection        int         `json:"default_branch_protection,omitempty"`
	AvatarURL                      string      `json:"avatar_url,omitempty"`
	WebURL                         string      `json:"web_url,omitempty"`
	RequestAccessEnabled           bool        `json:"request_access_enabled,omitempty"`
	FullName                       string      `json:"full_name,omitempty"`
	FullPath                       string      `json:"full_path,omitempty"`
	FileTemplateProjectID          int         `json:"file_template_project_id,omitempty"`
	ParentID                       interface{} `json:"parent_id,omitempty"`
	CreatedAt                      time.Time   `json:"created_at,omitempty"`
	Statistics                     Statistics  `json:"statistics,omitempty"`
}

type Statistics struct {
	StorageSize           int `json:"storage_size"`
	RepositorySize        int `json:"repository_size"`
	WikiSize              int `json:"wiki_size"`
	LfsObjectsSize        int `json:"lfs_objects_size"`
	JobArtifactsSize      int `json:"job_artifacts_size"`
	PipelineArtifactsSize int `json:"pipeline_artifacts_size"`
	PackagesSize          int `json:"packages_size"`
	SnippetsSize          int `json:"snippets_size"`
	UploadsSize           int `json:"uploads_size"`
}
