package listgroups

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/dualwield/glab/constructor"
	"gitlab.com/dualwield/glab/groups"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
)

const customStrAtt = "custom_attributes[%s]=%s"
const customIntAtt = "custom_attributes[%s]=%d"

type Call struct {
	constructor.API
	keyvals KeyVals
	page    int
}

type KeyVals map[string]bool

func (k KeyVals) Flatten() string {
	if len(k) == 0 {
		return ""
	}

	var set []string
	for k, _ := range k {
		set = append(set, k)
	}
	return "&" + strings.Join(set, "&")
}

func New(api constructor.API) Call {
	return Call{
		API:     api,
		keyvals: map[string]bool{},
		page:    1,
	}
}

func (c Call) Endpoint() string {
	const query = "%s/groups?per_page=5&page=%d%s"
	url := fmt.Sprintf(query, c.URL, c.page, c.keyvals.Flatten())
	url = strings.ReplaceAll(url, "?&", "?")
	return url
}

func (c Call) Paginate() (gxs []groups.Group, err error) {
	for {
		gxsz, cz, errz := c.Send()
		if errz != nil {
			return nil, errz
		}
		gxs = append(gxs, gxsz...)
		if cz != nil {
			c = *cz
		} else {
			return
		}
	}
}

func (c Call) Send() ([]groups.Group, *Call, error) {
	if rq, err := http.NewRequest(http.MethodGet, c.Endpoint(), nil); err != nil {
		return nil, nil, errors.Wrap(err, "fail creating http request [groups/listgroups]]")
	} else {
		rq.Header.Add("PRIVATE-TOKEN", c.Token)
		rs, err := c.Client.Do(rq)
		if err != nil {
			return nil, nil, errors.Wrap(err, "fail http GET request [groups/listgroups]")
		}
		if rs.StatusCode != http.StatusOK {
			return nil, nil, errors.New(fmt.Sprintf("status code %d", rs.StatusCode))
		}

		var next bool
		if nextPage := rs.Header.Get("x-next-page"); nextPage != "" {
			if page, err := strconv.Atoi(nextPage); err != nil {
				return nil, nil, errors.Wrap(err, "failure converting nextpage to int")
			} else {
				c.page = page
				next = true
			}
		}

		b, err := ioutil.ReadAll(rs.Body)
		if err != nil {
			return nil, nil, errors.Wrap(err, "fail [groups/listgroups] reading http response body")
		}
		defer rs.Body.Close()

		var gxs []groups.Group
		if err := json.Unmarshal(b, &gxs); err != nil {
			return nil, nil, errors.Wrap(err, "failed [groups/listgroups] at payload unmarshall")
		}
		if next {
			return gxs, &c, nil
		}
		return gxs, nil, nil
	}
}

func (c Call) Custom(key, val string) Call {
	c.keyvals[fmt.Sprintf(customStrAtt, key, val)] = true
	return c
}

func (c Call) WithStatistics() Call {
	c.keyvals["statistics=true"] = true
	return c
}

func (c Call) ParentID(id int) Call {
	c.keyvals[fmt.Sprintf(customIntAtt, "parent_id", id)] = true
	return c
}
